import { VStack, FormLabel } from "@chakra-ui/react";
import { toRem } from "@utils";
import React from "react";

interface IFormWrapper {
	label?: string;
	children?: React.ReactNode;
}

export function FormWrapper({
	label,
	children,
}: IFormWrapper): React.ReactElement {
	return (
		<VStack spacing={2} align="stretch">
			{label && (
				<FormLabel m={0} fontSize={toRem(13)} lineHeight={toRem(18)}>
					{label}
				</FormLabel>
			)}
			{children}
		</VStack>
	);
}
