import { InputGroup, Input, InputRightElement } from "@chakra-ui/react";
import { useCallback, useState } from "react";
import { RiEyeOffLine, RiEyeLine } from "react-icons/ri";

export function Password() {
	const [showPassword, setShowPassword] = useState(false);

	const togglePassword = useCallback(
		() => setShowPassword((current) => !current),
		[]
	);

	return (
		<InputGroup>
			<Input type={showPassword ? "text" : "password"} />
			<InputRightElement
				w={6}
				h={6}
				top={2.5}
				right={4}
				cursor="pointer"
				color="accent.primary"
				fontSize="3xl"
				onClick={togglePassword}
			>
				{showPassword ? <RiEyeOffLine /> : <RiEyeLine />}
			</InputRightElement>
		</InputGroup>
	);
}
