import {
	Box,
	Button,
	Heading,
	HStack,
	Input,
	Select,
	Tab,
	TabList,
	TabPanel,
	TabPanels,
	Tabs,
	Text,
	VStack,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useCallback, useEffect, useState } from "react";
import dayjs from "dayjs";

import { FormWrapper, Password } from "@components";
import { BaseLayout } from "@layout";
import { toRem } from "@utils";
import { CaretDownIcon, DownloadIcon } from "@icons";

export default function Home() {
	const [tab, setTab] = useState(0);
	const router = useRouter();

	useEffect(() => {
		const { query } = router;

		if (query && query.tab === "register") {
			setTab(1);
		}
	}, [router]);

	const onTabChange = useCallback(
		(val: number) => {
			router.push({
				query: {
					tab: val === 0 ? "login" : "register",
				},
			});
			setTab(val);
		},
		[router]
	);

	return (
		<BaseLayout>
			<Box p={24}>
				<Text fontSize="sm" lineHeight={4} color="basic.secondary" mb={8}>
					Today {dayjs().format("MMM DD, YYYY")}
				</Text>
				<Tabs onChange={onTabChange} index={tab}>
					<TabList>
						<Tab>Login</Tab>
						<Tab>Register</Tab>
					</TabList>
					<TabPanels>
						<TabPanel>
							<VStack spacing={8} align="stretch">
								<Heading as="h2" fontSize="lg" fontWeight={600} lineHeight={6}>
									Login Account
								</Heading>
								<FormWrapper label="Phone Number">
									<Input type="text" />
								</FormWrapper>
								<FormWrapper label="Password">
									<Password />
								</FormWrapper>
							</VStack>
						</TabPanel>
						<TabPanel>
							<VStack spacing={8} align="stretch">
								<Box>
									<Heading
										as="h2"
										fontSize="lg"
										fontWeight={600}
										lineHeight={6}
										mb={0.5}
									>
										Create New Account
									</Heading>
									<Text
										fontSize="xs"
										fontWeight={300}
										lineHeight={4}
										color="basic.secondary"
									>
										Before you can invest here, please create new account
									</Text>
								</Box>
								<Box>
									<Heading
										as="h2"
										fontSize="lg"
										fontWeight={600}
										lineHeight={6}
										mb={toRem(9)}
									>
										Account Detail
									</Heading>
									<VStack spacing={6} align="stretch">
										<FormWrapper label="Select Country">
											<Select variant="flushed" icon={<CaretDownIcon />}>
												<option>Indonesia (+62)</option>
											</Select>
										</FormWrapper>
										<FormWrapper label="Phone Number">
											<Input type="text" />
										</FormWrapper>
										<FormWrapper label="Password">
											<Password />
										</FormWrapper>
									</VStack>
								</Box>
							</VStack>
						</TabPanel>
					</TabPanels>
				</Tabs>
				<HStack spacing={2} py={1} color="accent.primary" mt={8} h={8}>
					{tab === 1 && (
						<>
							<DownloadIcon />
							<Text fontFamily="roboto" fontWeight={500} lineHeight={6}>
								Terms and conditions
							</Text>
						</>
					)}
				</HStack>
				<HStack mt={8} spacing={4}>
					<Button variant="secondary">Reset</Button>
					<Button variant="primary">{tab === 0 ? "Login" : "Register"}</Button>
				</HStack>
			</Box>
		</BaseLayout>
	);
}
