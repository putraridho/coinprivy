import { createIcon } from "@chakra-ui/react";

export const CaretDownIcon = createIcon({
	displayName: "CaretDownIcon",
	viewBox: "0 0 24 24",
	path: <path d="M7 10L12 15L17 10H7Z" fill="currentColor" />,
});
