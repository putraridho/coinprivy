import { Box, Flex, Heading, HStack, Text, VStack } from "@chakra-ui/react";
import { toRem } from "@utils";
import Image from "next/image";

interface IBaseLayout {
	children: React.ReactNode;
}

export function BaseLayout({ children }: IBaseLayout) {
	return (
		<Flex minH="100vh">
			<Box
				w={toRem(560)}
				bgColor="background.secondary"
				p={24}
				color="basic.primary"
			>
				<HStack spacing={3} align="center" mb={16}>
					<Image src="/images/logo.png" alt="logo" width={24} height={24} />
					<Text fontFamily="raleway" lineHeight={toRem(18)}>
						COINPRIVY
					</Text>
				</HStack>
				<VStack spacing={4} pl={9}>
					<Heading as="h1" fontSize={toRem(32)} lineHeight={9}>
						Welcome to Coinprivy
					</Heading>
					<Text fontFamily="roboto" fontWeight={300} color="basic.secondary">
						is a secure platform that makes it easy to buy, sell, and store
						cryptocurrency like Bitcoin, Ethereum, and more. Based in the USA
					</Text>
				</VStack>
			</Box>
			<Box flex={1} bgColor="background.primary" color="basic.primary">
				{children}
			</Box>
		</Flex>
	);
}
