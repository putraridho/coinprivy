import { defineStyleConfig } from "@chakra-ui/react";
import { toRem } from "@utils/toRem";

export const Tabs = defineStyleConfig({
	baseStyle: {},
	variants: {
		line: {
			tablist: {
				borderBottomWidth: "1px",
				borderBottomColor: "basic.strokes",
				gap: 6,
			},
			tab: {
				position: "relative",
				color: "basic.thriary",
				fontSize: "lg",
				fontWeight: 500,
				lineHeight: 6,
				px: 4,
				py: 3,
				border: 0,
				_after: {
					content: '""',
					position: "absolute",
					left: 0,
					bottom: toRem(1),
					w: "full",
					h: 1,
					bg: "linear-gradient(90deg, #563EEE 0%, #5C7AE5 100%)",
					opacity: 0,
				},
				_selected: {
					borderBottom: 0,
					color: "basic.primary",
					_after: {
						opacity: 1,
					},
				},
				_active: {
					bg: "transparent",
				},
			},
			tabpanels: {
				mt: 16,
				padding: 8,
				borderWidth: "1px",
				borderStyle: "solid",
				borderColor: "basic.strokes",
				borderRadius: 8,
			},
			tabpanel: {
				p: 0,
			},
		},
	},
});
