import { toRem } from "@utils/toRem";

export const Select = {
	sizes: {
		md: {
			field: {
				h: toRem(44),
			},
		},
	},
	variants: {
		flushed: {
			field: {
				borderColor: "basic.strokes",
				fontFamily: "roboto",
				fontWeight: 400,
				px: 4,
				py: 2,
				_hover: {
					borderColor: "basic.primary",
				},
			},
			icon: {
				right: 0,
			},
		},
	},
	defaultProps: {
		focusBorderColor: "accent.primary",
	},
};
