import { Button } from "./button";
import { Input } from "./input";
import { Select } from "./select";
import { Tabs } from "./tab";

export const components = {
	Button,
	Input,
	Select,
	Tabs,
};
