import { defineStyleConfig } from "@chakra-ui/react";

export const Button = defineStyleConfig({
	sizes: {
		md: {
			px: 6,
			py: 3,
			fontSize: "md",
			lineHeight: 6,
		},
	},
	variants: {
		primary: {
			bg: "background.contrast",
			color: "background.primary",
			fontFamily: "roboto",
			fontWeight: 500,
			borderRadius: 4,
		},
		secondary: {
			borderWidth: 1,
			borderColor: "basic.thriary",
			borderRadius: 4,
			fontFamily: "roboto",
			fontWeight: 500,
		},
	},
});
