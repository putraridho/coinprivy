import { toRem } from "@utils/toRem";

export const Input = {
	sizes: {
		md: {
			field: {
				h: toRem(44),
			},
		},
	},
	variants: {
		outline: {
			field: {
				borderColor: "basic.strokes",
				borderRadius: 6,
				fontFamily: "roboto",
				fontWeight: 400,
				px: 4,
				py: 2,
				_hover: {
					borderColor: "basic.primary",
				},
			},
		},
	},
	defaultProps: {
		focusBorderColor: "accent.primary",
	},
};
