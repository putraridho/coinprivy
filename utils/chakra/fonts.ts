export const fonts = {
	raleway: '"Raleway", sans-serif',
	roboto: '"Roboto", sans-serif',
	poppins: '"Poppins", sans-serif',
	heading: '"Poppins", sans-serif',
	body: '"Poppins", sans-serif',
};
