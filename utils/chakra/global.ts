import { toRem } from "@utils/toRem";

export const global = {
	p: {
		fontSize: "md",
		lineHeight: toRem(22),
	},
};
