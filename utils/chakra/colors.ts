export const colors = {
	basic: {
		primary: "#F3F3F3",
		secondary: "#8E8EA3",
		thriary: "#818191",
		strokes: "#52526B",
	},
	accent: {
		primary: "#5C7AE5",
	},
	background: {
		primary: "#16162A",
		secondary: "#292946",
		contrast: "#EAEAFF",
	},
};
