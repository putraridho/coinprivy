import { extendTheme } from "@chakra-ui/react";
import { colors } from "./chakra/colors";
import { components } from "./chakra/components";
import { fonts } from "./chakra/fonts";
import { global } from "./chakra/global";

export const theme = extendTheme({
	colors,
	fonts,
	components,
	styles: {
		global,
	},
});
